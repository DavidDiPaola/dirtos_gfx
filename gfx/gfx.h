#ifndef _GFX_H
#define _GFX_H

#include <stdint.h>

#include "../mth/mth.h"

typedef uint16_t gfx_px16;
typedef int32_t gfx_crd;
struct gfx_v3d_qf {
	float angle;
	struct mth_v3f axis;
};

/* TODO
	WARNING: must be called before calling any drawing operations.
*/
void
gfx_init(gfx_px16 * buffer, gfx_crd buffer_width, gfx_crd buffer_height);

/* TODO finish this */
/* A quick and easy way to initialize the 3D renderer's parameters. */
void
gfx_v3d_easyinit(void);

/* TODO
	WARNING: must be called at least once before calling any 3D drawing operations.
*/
void
gfx_3d_init(float fov);

void
gfx_v3d_projection_set(float fov_y, float z_near, float z_far);

void
gfx_v3d_view_set(const struct mth_v3f * eye, const struct mth_v3f * center, const struct mth_v3f * up);

#ifdef GFX_DEBUG_3D_PXFILLED
uint64_t
gfx_3d_pxfilled_get(void);
#endif

void
gfx_3d_coord_flatten(const struct mth_v3f * c3d, struct mth_v2f * out_c2d);

void
gfx_3d_coord_flatten_array(const struct mth_v3f * c3d, struct mth_v2f * c2d, size_t length);

void
gfx_coord_unittoint(const struct mth_v2f * u, gfx_crd * out_x_px, gfx_crd * out_y_px);

void
gfx_print(gfx_crd x, gfx_crd y, const char * str);

void
gfx_print_int32(gfx_crd x, gfx_crd y, int32_t n);

void
gfx_print_hex32(gfx_crd x, gfx_crd y, uint32_t hex);

gfx_px16
gfx_color(uint8_t red, uint8_t green, uint8_t blue);

/*
	NOTE position must have length >= 3
*/
void
gfx_v2d_triangle(const struct mth_v2f * position, gfx_px16 color, int filled);

/*
	NOTE position must have length >= 3
*/
void
gfx_v3d_triangle(const struct mth_v3f * position, gfx_px16 color, int filled);

void
gfx_test(void);

#endif

