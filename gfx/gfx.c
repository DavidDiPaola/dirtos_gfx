#include <stddef.h>

#include <stdint.h>

#include <stdio.h>  /* TODO remove */

#include "../mth/mth.h"

#include "gfx.h"

static const uint8_t _font[];
static const gfx_crd _font_ch_width;
static const gfx_crd _font_ch_height;

static gfx_px16 * _buffer;
static gfx_crd    _buffer_width;
static gfx_crd    _buffer_height;
static float      _buffer_width_half;
static float      _buffer_height_half;
static float      _buffer_aspect;

static struct mth_m4f _v3d_mvp_projection;
static struct mth_m4f _v3d_mvp_view;
static float          _3d_z_scalar;  /* TODO remove */

#ifdef GFX_DEBUG_3D_PXFILLED
/* TODO use this in more places */
static uint64_t _3d_pxfilled;
#endif

void
gfx_init(gfx_px16 * buffer, gfx_crd buffer_width, gfx_crd buffer_height) {
	_buffer = buffer;
	_buffer_width  = buffer_width;
	_buffer_height = buffer_height;
	_buffer_width_half  = _buffer_width  / 2.0f;
	_buffer_height_half = _buffer_height / 2.0f;
	_buffer_aspect = (float)_buffer_width / _buffer_height;

	#ifdef GFX_DEBUG_3D_PXFILLED
	_3d_pxfilled = 0;
	#endif
}

/* TODO finish this */
void
gfx_v3d_easyinit(void) {
	gfx_v3d_projection_set(/*fov_y=*/90, /*z_near=*/0.1f, /*z_far=*/100.0f);

	struct mth_v3f eye    = { .x= 1.0f, .y= 1.0f, .z= 1.0f };
	struct mth_v3f center = { .x= 0.0f, .y= 0.0f, .z= 0.0f };
	struct mth_v3f up     = { .x= 0.0f, .y= 1.0f, .z= 0.0f };
	gfx_v3d_view_set(&eye, &center, &up);
}

void
gfx_v3d_projection_set(float fov_y, float z_near, float z_far) {
	/* See: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml */

	if (fov_y < 0.0f) {
		fprintf(stderr, "gfx_v3d_perspective_set() ERROR: fov_y < 0.0f (is %g)" "\n", fov_y);
		return;
	}
	if (_buffer_aspect < 0.0f) {
		fprintf(stderr, "gfx_v3d_perspective_set() ERROR: _buffer_aspect < 0.0f (is %g)" "\n", _buffer_aspect);
		return;
	}
	if (z_near < 0.0f) {
		fprintf(stderr, "gfx_v3d_perspective_set() ERROR: z_near < 0.0f (is %g)" "\n", z_near);
		return;
	}
	if (z_far < 0.0f) {
		fprintf(stderr, "gfx_v3d_perspective_set() ERROR: z_far < 0.0f (is %g)" "\n", z_far);
		return;
	}
	if (z_near > z_far) {
		fprintf(stderr, "gfx_v3d_perspective_set() ERROR: z_near > z_far (%g > %g)" "\n", z_near, z_far);
		return;
	}

	float f = 1.0f / mth_tan_f(fov_y / 2.0f);
	
	struct mth_m4f * m = &_v3d_mvp_projection;
	mth_m4f_AT_ptr(m,0,0)=f/_buffer_aspect;  mth_m4f_AT_ptr(m,0,1)=0.0f;  mth_m4f_AT_ptr(m,0,2)=0.0f;                           mth_m4f_AT_ptr(m,0,3)=0.0f;
	mth_m4f_AT_ptr(m,1,0)=0.0f;              mth_m4f_AT_ptr(m,1,1)=f;     mth_m4f_AT_ptr(m,1,2)=0.0f;                           mth_m4f_AT_ptr(m,1,3)=0.0f;
	mth_m4f_AT_ptr(m,2,0)=0.0f;              mth_m4f_AT_ptr(m,2,1)=0.0f;  mth_m4f_AT_ptr(m,2,2)=(z_far+z_near)/(z_near-z_far);  mth_m4f_AT_ptr(m,2,3)=(2*z_far*z_near)/(z_near-z_far);
	mth_m4f_AT_ptr(m,3,0)=0.0f;              mth_m4f_AT_ptr(m,3,1)=0.0f;  mth_m4f_AT_ptr(m,3,2)=-1.0f;                          mth_m4f_AT_ptr(m,3,3)=0.0f;
}

void
gfx_v3d_view_set(const struct mth_v3f * eye, const struct mth_v3f * center, const struct mth_v3f * up) {
	/* Equivelant to gluLookAt() followed by glTranslatef(). See: https://www.opengl.org/discussion_boards/showthread.php/130409-using-gluLookAt-properly */
	/* See OpenGL 2.1 gluLookAt(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml */
	/* See OpenGL 2.1 glTranslatef(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glTranslate.xml */

	struct mth_v3f f = {
		.x = center->x - eye->x,
		.y = center->y - eye->y,
		.z = center->z - eye->z,
	};
	struct mth_v3f f_norm;
	mth_v3f_normal(&f, &f_norm);

	struct mth_v3f up_norm;
	mth_v3f_normal(up, &up_norm);

	struct mth_v3f s;
	mth_v3f_cross(&f_norm, &up_norm, &s);

	struct mth_v3f s_norm;
	mth_v3f_normal(&s, &s_norm);

	struct mth_v3f u;
	mth_v3f_cross(&s_norm, &f_norm, &u);

	float eye_translate_x;
	mth_v3f_dot(&s_norm, eye, &eye_translate_x);
	eye_translate_x *= -1.0f;

	float eye_translate_y;
	mth_v3f_dot(&u, eye, &eye_translate_y);
	eye_translate_y *= -1.0f;

	float eye_translate_z;
	mth_v3f_dot(&f_norm, eye, &eye_translate_z);

	struct mth_m4f * m = &_v3d_mvp_view;
	mth_m4f_AT_ptr(m,0,0)=s.x;        mth_m4f_AT_ptr(m,0,1)=s.y;        mth_m4f_AT_ptr(m,0,2)=s.z;        mth_m4f_AT_ptr(m,0,3)=eye_translate_x;
	mth_m4f_AT_ptr(m,1,0)=u.x;        mth_m4f_AT_ptr(m,1,1)=u.y;        mth_m4f_AT_ptr(m,1,2)=u.z;        mth_m4f_AT_ptr(m,1,3)=eye_translate_y;
	mth_m4f_AT_ptr(m,2,0)=-f_norm.x;  mth_m4f_AT_ptr(m,2,1)=-f_norm.y;  mth_m4f_AT_ptr(m,2,2)=-f_norm.z;  mth_m4f_AT_ptr(m,2,3)=eye_translate_z;
	mth_m4f_AT_ptr(m,3,0)=0.0f;       mth_m4f_AT_ptr(m,3,1)=0.0f;       mth_m4f_AT_ptr(m,3,2)=0.0f;       mth_m4f_AT_ptr(m,3,3)=1.0f;
}

void
gfx_v3d_rotate(
	const struct mth_m4f * matrix, const struct gfx_v3d_qf * rotation,
	struct mth_m4f * out_result
) {
	/* See OpenGL 2.1 glRotate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glRotate.xml */
	/* NOTE glRotate() was deprecated, then removed from later OpenGL versions. Don't use it. */

	struct mth_v3f axis;
	mth_v3f_normal(&(rotation->axis), &axis);
	float x = rotation->axis.x;
	float y = rotation->axis.y;
	float z = rotation->axis.z;
	float c = mth_cos_f(rotation->angle);
	float s = mth_sin_f(rotation->angle);
	struct mth_m4f rotation_matrix;
	struct mth_m4f * r = &rotation_matrix;
	mth_m4f_AT_ptr(r,0,0)=(x*x*(1-c))+(  c);  mth_m4f_AT_ptr(r,0,1)=(x*y*(1-c))-(z*s);  mth_m4f_AT_ptr(r,0,2)=(x*z*(1-c))+(y*s);  mth_m4f_AT_ptr(r,0,3)=0.0f;
	mth_m4f_AT_ptr(r,1,0)=(y*x*(1-c))+(z*s);  mth_m4f_AT_ptr(r,1,1)=(y*y*(1-c))+(  c);  mth_m4f_AT_ptr(r,1,2)=(y*z*(1-c))-(x*s);  mth_m4f_AT_ptr(r,1,3)=0.0f;
	mth_m4f_AT_ptr(r,2,0)=(x*z*(1-c))-(y*s);  mth_m4f_AT_ptr(r,2,1)=(y*z*(1-c))+(x*s);  mth_m4f_AT_ptr(r,2,2)=(z*z*(1-c))+(  c);  mth_m4f_AT_ptr(r,2,3)=0.0f;
	mth_m4f_AT_ptr(r,3,0)=0.0f;               mth_m4f_AT_ptr(r,3,1)=0.0f;               mth_m4f_AT_ptr(r,3,2)=0.0f;               mth_m4f_AT_ptr(r,3,3)=1.0f;
	mth_mult_m4f(matrix, &rotation_matrix, out_result);
}

/* TODO remove */
void
gfx_3d_init(float fov) {
	_3d_z_scalar = 1.0f / mth_tan_f(mth_degtorad_f(fov) / 2.0f);
}

#ifdef GFX_DEBUG_3D_PXFILLED
uint64_t
gfx_3d_pxfilled_get(void) {
	return _3d_pxfilled;
}
#endif

void
gfx_3d_coord_flatten(const struct mth_v3f * c3d, struct mth_v2f * out_c2d) {
	float c3d_z_scaled = c3d->z * _3d_z_scalar;
	out_c2d->x = c3d->x / c3d_z_scaled;
	out_c2d->y = c3d->y / c3d_z_scaled;
}

void
gfx_3d_coord_flatten_array(const struct mth_v3f * c3d, struct mth_v2f * c2d, size_t length) {
	for (size_t i=0; i<length; i++,c3d++,c2d++) {
		gfx_3d_coord_flatten(c3d, c2d);
	}
}

void
gfx_coord_unittoint(const struct mth_v2f * u, gfx_crd * out_x_px, gfx_crd * out_y_px) {
	float u_y_flipped = u->y * -1.0f;
	/* top-left pixel/fill convention */
	(*out_x_px) = mth_ceil_f((u->x        * _buffer_width ) + _buffer_width_half );
	(*out_y_px) = mth_ceil_f((u_y_flipped * _buffer_height) + _buffer_height_half);
}

gfx_px16
gfx_color(uint8_t red, uint8_t green, uint8_t blue) {
	return
		((red  >>3)<<11) |
		((green>>2)<< 5) |
		((blue >>3)<< 0)
	;
}

/* "sorted" means a_y < b_y */
static void
_line_sorted(
	gfx_crd a_x, gfx_crd a_y,
	gfx_crd b_x, gfx_crd b_y,
	gfx_px16 color
) {
	gfx_crd dy = b_y - a_y;
	if (dy > 0) {
		float x = a_x;
		gfx_px16 * row = _buffer + (a_y * _buffer_width);
		float slope = ((float)b_x - a_x) / dy;
		for (gfx_crd i=0; i<dy; i++) {
			row[(int)x] = color;

			x += slope;
			row += _buffer_width;
		}
	}
}

void
gfx_line(
	gfx_crd a_x, gfx_crd a_y,
	gfx_crd b_x, gfx_crd b_y,
	gfx_px16 color
) {
	if (a_y > b_y) {
		gfx_crd temp_x = a_x;
		gfx_crd temp_y = a_y;
		a_x = b_x;
		a_y = b_y;
		b_x = temp_x;
		b_y = temp_y;
	}
	_line_sorted(a_x, a_y, b_x, b_y, color);
}

/* TODO debug ifdef */
/* Naive fill implementation. Only intended for test use. */
static void
_rowfill_cpu16(gfx_px16 * row, gfx_crd left, gfx_crd right, gfx_px16 color) {
	while (left != right) {
		row[left] = color;
		left++;
	}
}

/* TODO CPU ifdef */
/* for >=32bit CPUs? */
static void
_rowfill_cpu32(gfx_px16 * row, gfx_crd left, gfx_crd right, gfx_px16 color) {
	uint32_t    block = (((uint32_t)color)<<16)|color;
	uint32_t  * block_row = (uint32_t *)row;
	gfx_crd   block_step = sizeof(block) / sizeof(gfx_crd);

	/* before aligned: naive fill */
	while ((left % block_step) != 0) {
		row[left] = color;
		left++;
	}

	/* while aligned: block fill */
	size_t block_left  = left  / block_step;
	size_t block_right = right / block_step;
	while (block_left != block_right) {
		block_row[block_left] = block;
		block_left++;
	}
	left = block_left * block_step;

	/* after aligned: naive fill */
	while (left != right) {
		row[left] = color;
		left++;
	}
}

/* TODO CPU ifdef */
/* for >=64bit CPUs? */
static void
_rowfill_cpu64(gfx_px16 * row, gfx_crd left, gfx_crd right, gfx_px16 color) {
	uint64_t    block = (((uint64_t)color)<<48)|(((uint64_t)color)<<32)|(((uint64_t)color)<<16)|color;
	uint64_t  * block_row = (uint64_t *)row;
	gfx_crd   block_step = sizeof(block) / sizeof(gfx_crd);

	/* before aligned: naive fill */
	while ((left % block_step) != 0) {
		row[left] = color;
		left++;
	}

	/* while aligned: block fill */
	size_t block_left  = left  / block_step;
	size_t block_right = right / block_step;
	while (block_left != block_right) {
		block_row[block_left] = block;
		block_left++;
	}
	left = block_left * block_step;

	/* after aligned: naive fill */
	while (left != right) {
		row[left] = color;
		left++;
	}
}

static void
_triangle_part(
	float * inout_x_left, float * inout_x_right,
	float * inout_slope_left, float * inout_slope_right,
	gfx_crd y_start, gfx_crd y_end,
	gfx_px16 color,
	int filled
) {
	float x_left  = *inout_x_left;
	float x_right = *inout_x_right;
	float slope_left  = *inout_slope_left;
	float slope_right = *inout_slope_right;

	//printf("DEBUG: %s(): args: slope_left=%f slope_right=%f" "\n", __func__, slope_left, slope_right);

	for (gfx_crd y=y_start; y<y_end; y++) {
		//printf("DEBUG: %s(): v loop: x_left=%f x_right=%f" "\n", __func__, x_left, x_right);

		if ((y>=0) && (y<_buffer_height)) {
			gfx_px16 * row = _buffer+(_buffer_width*y);

			/* horizontal fill convention: left */
			gfx_crd x_left_crd  = mth_ceil_f(x_left);
			gfx_crd x_right_crd = mth_ceil_f(x_right);
			if (filled) {
				if ((x_left_crd<_buffer_width) && (x_right_crd>=0)) {
					x_left_crd  = (x_left_crd < 0) ? 0 : x_left_crd;
					x_right_crd = (x_right_crd > (_buffer_width-1)) ? (_buffer_width-1) : x_right_crd;

					#ifdef GFX_DEBUG_3D_PXFILLED
					_3d_pxfilled += x_right_crd - x_left_crd;
					#endif

					//printf("DEBUG: %s(): fill: x_left_crd=%i (%f), x_right=%i (%f)" "\n", __func__, x_left_crd, x_left, x_right_crd, x_right);
					/* TODO CPU ifdef */
					_rowfill_cpu16(row, x_left_crd, x_right_crd, color);
					//_rowfill_cpu32(row, x_left_crd, x_right_crd, color);
					//_rowfill_cpu64(row, x_left_crd, x_right_crd, color);
				}
			}
			else {
				if ((x_left_crd>=0) && (x_left_crd<_buffer_width)) {
					row[x_left_crd]  = color;
				}
				if ((x_right_crd>=0) && (x_right_crd<_buffer_width)) {
					row[x_right_crd] = color;
				}
			}
		}
		
		x_left  += slope_left;
		x_right += slope_right;
	}

	*inout_x_left  = x_left;
	*inout_x_right = x_right;
	*inout_slope_left  = slope_left;
	*inout_slope_right = slope_right;
}

static void
_triangle_sorted(
	gfx_crd a_x, gfx_crd a_y, gfx_crd b_x, gfx_crd b_y, gfx_crd c_x, gfx_crd c_y,
	gfx_px16 color,
	int filled
) {
	float x_left  = a_x;
	float x_right = a_x;

	gfx_crd top_y_start = a_y;
	gfx_crd top_y_end   = b_y;
	gfx_crd top_dy      = top_y_end - top_y_start;
	gfx_crd bottom_y_start = top_y_end;
	gfx_crd bottom_y_end   = c_y;
	gfx_crd bottom_dy      = bottom_y_end - bottom_y_start;

	if (b_x < c_x) {  /* bend on left side */
		/*
		   A
		 / |
		B  |
		 \ |
		   C
		*/

		float top_slope_right = ((float)c_x - a_x) / (c_y - a_y);

		if (top_dy > 0) {
			float top_slope_left  = ((float)b_x - a_x) / top_dy;
			_triangle_part(
				&x_left, &x_right,
				&top_slope_left, &top_slope_right,
				top_y_start, top_y_end,
				color,
				filled
			);
		}

		if (bottom_dy > 0) {
			float bottom_slope_left  = ((float)c_x - b_x) / bottom_dy;
			float bottom_slope_right = top_slope_right;
			_triangle_part(
				&x_left, &x_right,
				&bottom_slope_left, &bottom_slope_right,
				bottom_y_start, bottom_y_end,
				color,
				filled
			);
		}
	}
	else {  /* bend on right side, or flat bottom */
		/*
		A           A
		| \        / \
		|  B  or  B---C
		| /
		C
		*/

		float top_slope_left  = ((float)c_x - a_x) / (c_y - a_y);

		if (top_dy > 0) {
			float top_slope_right = ((float)b_x - a_x) / top_dy;
			_triangle_part(
				&x_left, &x_right,
				&top_slope_left, &top_slope_right,
				top_y_start, top_y_end,
				color,
				filled
			);
		}

		if (bottom_dy > 0) {
			float bottom_slope_left  = top_slope_left;
			float bottom_slope_right = ((float)c_x - b_x) / bottom_dy;
			_triangle_part(
				&x_left, &x_right,
				&bottom_slope_left, &bottom_slope_right,
				bottom_y_start, bottom_y_end,
				color,
				filled
			);
		}
	}
}

void
gfx_v2d_triangle(const struct mth_v2f * position, gfx_px16 color, int filled) {
	gfx_crd a_x, a_y;
	gfx_coord_unittoint(position+0, &a_x, &a_y);
	gfx_crd b_x, b_y;
	gfx_coord_unittoint(position+1, &b_x, &b_y);
	gfx_crd c_x, c_y;
	gfx_coord_unittoint(position+2, &c_x, &c_y);

	if (a_y > b_y) {
		gfx_crd temp_x = a_x;
		gfx_crd temp_y = a_y;
		a_x = b_x;
		a_y = b_y;
		b_x = temp_x;
		b_y = temp_y;
	}
	if (b_y > c_y) {
		gfx_crd temp_x = b_x;
		gfx_crd temp_y = b_y;
		b_x = c_x;
		b_y = c_y;
		c_x = temp_x;
		c_y = temp_y;

		if (a_y > b_y) {
			temp_x = a_x;
			temp_y = a_y;
			a_x = b_x;
			a_y = b_y;
			b_x = temp_x;
			b_y = temp_y;
		}
	}
	_triangle_sorted(
		a_x, a_y,  b_x, b_y,  c_x, c_y,
		color,
		filled
	);
}

void
gfx_v3d_triangle(const struct mth_v3f * position, gfx_px16 color, int filled) {
	struct mth_v2f position_flat[3];
	gfx_3d_coord_flatten_array(position, position_flat, 3);
	gfx_v2d_triangle(position_flat, color, filled);
}

void
gfx_print_char(gfx_crd x, gfx_crd y, char ch) {
	const uint8_t * font_row = _font + (((unsigned)ch)*8);
	for (gfx_crd font_y=0; font_y<_font_ch_height; font_y++,font_row++) {
		uint8_t font_px = *font_row;
		for (gfx_crd font_x=0; font_x<_font_ch_width; font_x++,font_px<<=1) {
			gfx_px16 * buffer_px = _buffer + (x+font_x) + ((y+font_y)*_buffer_width);
			if (font_px & (1<<7)) {
				*buffer_px = 0xFFFF;
			}
		}
	}
}

void
gfx_print(gfx_crd x, gfx_crd y, const char * str) {
	for (; (*str)!='\0'; str++,x+=8) {
		gfx_print_char(x, y, *str);
	}
}

void
gfx_print_int32(gfx_crd x, gfx_crd y, int32_t n) {
	if (n == 0) {
		gfx_print_char(x, y, '0');
		return;
	}

	if (n < 0) {
		gfx_print_char(x, y, '-');
		x += _font_ch_width;
		n *= -1;
	}

	int printed = 0;
	int32_t div = 1000000000;
	for (size_t i=0; i<10; i++,div/=10) { 
		int digit = (n / div) % 10;
		if (digit < 0) {  /* HACK when GCC is at -O2 digit can go negative */
			return;
		}
		if ((digit!=0) || printed) {
			gfx_print_char(x, y, '0'+digit);
			x += _font_ch_width;
			printed = 1;
		}
	}
}

void
gfx_print_hex32(gfx_crd x, gfx_crd y, uint32_t hex) {
	gfx_print(x, y, "0x");
	x += _font_ch_width * 2;

	for (int i=32-4; i>=0; i-=4) {
		uint8_t nibble = (hex >> i) & 0xF;
		uint8_t offset;
		if (nibble <= 9) {
			offset = '0';
		}
		else {
			offset = 'A';
			nibble -= 0xA;
		}
		gfx_print_char(x, y, offset+nibble);
		x += _font_ch_width;
	}
}

void
gfx_test(void) {
}

static const gfx_crd _font_ch_width  = 8;
static const gfx_crd _font_ch_height = 8;
static const uint8_t _font[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x3E, 0x41, 0x55, 0x41, 0x55, 0x49, 0x3E, 
	0x00, 0x3E, 0x7F, 0x6B, 0x7F, 0x6B, 0x77, 0x3E, 
	0x00, 0x22, 0x77, 0x7F, 0x7F, 0x3E, 0x1C, 0x08, 
	0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x3E, 0x1C, 0x08, 
	0x00, 0x08, 0x1C, 0x2A, 0x7F, 0x2A, 0x08, 0x1C, 
	0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x3E, 0x08, 0x1C, 
	0x00, 0x00, 0x1C, 0x3E, 0x3E, 0x3E, 0x1C, 0x00, 
	0xFF, 0xFF, 0xE3, 0xC1, 0xC1, 0xC1, 0xE3, 0xFF, 
	0x00, 0x00, 0x1C, 0x22, 0x22, 0x22, 0x1C, 0x00, 
	0xFF, 0xFF, 0xE3, 0xDD, 0xDD, 0xDD, 0xE3, 0xFF, 
	0x00, 0x0F, 0x03, 0x05, 0x39, 0x48, 0x48, 0x30, 
	0x00, 0x08, 0x3E, 0x08, 0x1C, 0x22, 0x22, 0x1C, 
	0x00, 0x18, 0x14, 0x10, 0x10, 0x30, 0x70, 0x60, 
	0x00, 0x0F, 0x19, 0x11, 0x13, 0x37, 0x76, 0x60, 
	0x00, 0x08, 0x2A, 0x1C, 0x77, 0x1C, 0x2A, 0x08, 
	0x00, 0x60, 0x78, 0x7E, 0x7F, 0x7E, 0x78, 0x60, 
	0x00, 0x03, 0x0F, 0x3F, 0x7F, 0x3F, 0x0F, 0x03, 
	0x00, 0x08, 0x1C, 0x2A, 0x08, 0x2A, 0x1C, 0x08, 
	0x00, 0x66, 0x66, 0x66, 0x66, 0x00, 0x66, 0x66, 
	0x00, 0x3F, 0x65, 0x65, 0x3D, 0x05, 0x05, 0x05, 
	0x00, 0x0C, 0x32, 0x48, 0x24, 0x12, 0x4C, 0x30, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0x7F, 0x7F, 
	0x00, 0x08, 0x1C, 0x2A, 0x08, 0x2A, 0x1C, 0x3E, 
	0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x1C, 0x1C, 0x1C, 
	0x00, 0x1C, 0x1C, 0x1C, 0x7F, 0x3E, 0x1C, 0x08, 
	0x00, 0x08, 0x0C, 0x7E, 0x7F, 0x7E, 0x0C, 0x08, 
	0x00, 0x08, 0x18, 0x3F, 0x7F, 0x3F, 0x18, 0x08, 
	0x00, 0x00, 0x00, 0x70, 0x70, 0x70, 0x7F, 0x7F, 
	0x00, 0x00, 0x14, 0x22, 0x7F, 0x22, 0x14, 0x00, 
	0x00, 0x08, 0x1C, 0x1C, 0x3E, 0x3E, 0x7F, 0x7F, 
	0x00, 0x7F, 0x7F, 0x3E, 0x3E, 0x1C, 0x1C, 0x08, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x18, 0x3C, 0x3C, 0x18, 0x18, 0x00, 0x18, 
	0x00, 0x36, 0x36, 0x14, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x36, 0x36, 0x7F, 0x36, 0x7F, 0x36, 0x36, 
	0x00, 0x08, 0x1E, 0x20, 0x1C, 0x02, 0x3C, 0x08, 
	0x00, 0x60, 0x66, 0x0C, 0x18, 0x30, 0x66, 0x06, 
	0x00, 0x3C, 0x66, 0x3C, 0x28, 0x65, 0x66, 0x3F, 
	0x00, 0x18, 0x18, 0x18, 0x30, 0x00, 0x00, 0x00, 
	0x00, 0x60, 0x30, 0x18, 0x18, 0x18, 0x30, 0x60, 
	0x00, 0x06, 0x0C, 0x18, 0x18, 0x18, 0x0C, 0x06, 
	0x00, 0x00, 0x36, 0x1C, 0x7F, 0x1C, 0x36, 0x00, 
	0x00, 0x00, 0x08, 0x08, 0x3E, 0x08, 0x08, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x60, 
	0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x60, 
	0x00, 0x00, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x00, 
	0x00, 0x3C, 0x66, 0x6E, 0x76, 0x66, 0x66, 0x3C, 
	0x00, 0x18, 0x18, 0x38, 0x18, 0x18, 0x18, 0x7E, 
	0x00, 0x3C, 0x66, 0x06, 0x0C, 0x30, 0x60, 0x7E, 
	0x00, 0x3C, 0x66, 0x06, 0x1C, 0x06, 0x66, 0x3C, 
	0x00, 0x0C, 0x1C, 0x2C, 0x4C, 0x7E, 0x0C, 0x0C, 
	0x00, 0x7E, 0x60, 0x7C, 0x06, 0x06, 0x66, 0x3C, 
	0x00, 0x3C, 0x66, 0x60, 0x7C, 0x66, 0x66, 0x3C, 
	0x00, 0x7E, 0x66, 0x0C, 0x0C, 0x18, 0x18, 0x18, 
	0x00, 0x3C, 0x66, 0x66, 0x3C, 0x66, 0x66, 0x3C, 
	0x00, 0x3C, 0x66, 0x66, 0x3E, 0x06, 0x66, 0x3C, 
	0x00, 0x00, 0x18, 0x18, 0x00, 0x18, 0x18, 0x00, 
	0x00, 0x00, 0x18, 0x18, 0x00, 0x18, 0x18, 0x30, 
	0x00, 0x06, 0x0C, 0x18, 0x30, 0x18, 0x0C, 0x06, 
	0x00, 0x00, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x00, 
	0x00, 0x60, 0x30, 0x18, 0x0C, 0x18, 0x30, 0x60, 
	0x00, 0x3C, 0x66, 0x06, 0x1C, 0x18, 0x00, 0x18, 
	0x00, 0x38, 0x44, 0x5C, 0x58, 0x42, 0x3C, 0x00, 
	0x00, 0x3C, 0x66, 0x66, 0x7E, 0x66, 0x66, 0x66, 
	0x00, 0x7C, 0x66, 0x66, 0x7C, 0x66, 0x66, 0x7C, 
	0x00, 0x3C, 0x66, 0x60, 0x60, 0x60, 0x66, 0x3C, 
	0x00, 0x7C, 0x66, 0x66, 0x66, 0x66, 0x66, 0x7C, 
	0x00, 0x7E, 0x60, 0x60, 0x7C, 0x60, 0x60, 0x7E, 
	0x00, 0x7E, 0x60, 0x60, 0x7C, 0x60, 0x60, 0x60, 
	0x00, 0x3C, 0x66, 0x60, 0x60, 0x6E, 0x66, 0x3C, 
	0x00, 0x66, 0x66, 0x66, 0x7E, 0x66, 0x66, 0x66, 
	0x00, 0x3C, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3C, 
	0x00, 0x1E, 0x0C, 0x0C, 0x0C, 0x6C, 0x6C, 0x38, 
	0x00, 0x66, 0x6C, 0x78, 0x70, 0x78, 0x6C, 0x66, 
	0x00, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x7E, 
	0x00, 0x63, 0x77, 0x7F, 0x6B, 0x63, 0x63, 0x63, 
	0x00, 0x63, 0x73, 0x7B, 0x6F, 0x67, 0x63, 0x63, 
	0x00, 0x3C, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 
	0x00, 0x7C, 0x66, 0x66, 0x66, 0x7C, 0x60, 0x60, 
	0x00, 0x3C, 0x66, 0x66, 0x66, 0x6E, 0x3C, 0x06, 
	0x00, 0x7C, 0x66, 0x66, 0x7C, 0x78, 0x6C, 0x66, 
	0x00, 0x3C, 0x66, 0x60, 0x3C, 0x06, 0x66, 0x3C, 
	0x00, 0x7E, 0x5A, 0x18, 0x18, 0x18, 0x18, 0x18, 
	0x00, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3E, 
	0x00, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x18, 
	0x00, 0x63, 0x63, 0x63, 0x6B, 0x7F, 0x77, 0x63, 
	0x00, 0x63, 0x63, 0x36, 0x1C, 0x36, 0x63, 0x63, 
	0x00, 0x66, 0x66, 0x66, 0x3C, 0x18, 0x18, 0x18, 
	0x00, 0x7E, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x7E, 
	0x00, 0x1E, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1E, 
	0x00, 0x00, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x00, 
	0x00, 0x78, 0x18, 0x18, 0x18, 0x18, 0x18, 0x78, 
	0x00, 0x08, 0x14, 0x22, 0x41, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 
	0x00, 0x0C, 0x0C, 0x06, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x3C, 0x06, 0x3E, 0x66, 0x3E, 
	0x00, 0x60, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x7C, 
	0x00, 0x00, 0x00, 0x3C, 0x66, 0x60, 0x66, 0x3C, 
	0x00, 0x06, 0x06, 0x06, 0x3E, 0x66, 0x66, 0x3E, 
	0x00, 0x00, 0x00, 0x3C, 0x66, 0x7E, 0x60, 0x3C, 
	0x00, 0x1C, 0x36, 0x30, 0x30, 0x7C, 0x30, 0x30, 
	0x00, 0x00, 0x3E, 0x66, 0x66, 0x3E, 0x06, 0x3C, 
	0x00, 0x60, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x66, 
	0x00, 0x00, 0x18, 0x00, 0x18, 0x18, 0x18, 0x3C, 
	0x00, 0x0C, 0x00, 0x0C, 0x0C, 0x6C, 0x6C, 0x38, 
	0x00, 0x60, 0x60, 0x66, 0x6C, 0x78, 0x6C, 0x66, 
	0x00, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 
	0x00, 0x00, 0x00, 0x63, 0x77, 0x7F, 0x6B, 0x6B, 
	0x00, 0x00, 0x00, 0x7C, 0x7E, 0x66, 0x66, 0x66, 
	0x00, 0x00, 0x00, 0x3C, 0x66, 0x66, 0x66, 0x3C, 
	0x00, 0x00, 0x7C, 0x66, 0x66, 0x7C, 0x60, 0x60, 
	0x00, 0x00, 0x3C, 0x6C, 0x6C, 0x3C, 0x0D, 0x0F, 
	0x00, 0x00, 0x00, 0x7C, 0x66, 0x66, 0x60, 0x60, 
	0x00, 0x00, 0x00, 0x3E, 0x40, 0x3C, 0x02, 0x7C, 
	0x00, 0x00, 0x18, 0x18, 0x7E, 0x18, 0x18, 0x18, 
	0x00, 0x00, 0x00, 0x66, 0x66, 0x66, 0x66, 0x3E, 
	0x00, 0x00, 0x00, 0x00, 0x66, 0x66, 0x3C, 0x18, 
	0x00, 0x00, 0x00, 0x63, 0x6B, 0x6B, 0x6B, 0x3E, 
	0x00, 0x00, 0x00, 0x66, 0x3C, 0x18, 0x3C, 0x66, 
	0x00, 0x00, 0x00, 0x66, 0x66, 0x3E, 0x06, 0x3C, 
	0x00, 0x00, 0x00, 0x3C, 0x0C, 0x18, 0x30, 0x3C, 
	0x00, 0x0E, 0x18, 0x18, 0x30, 0x18, 0x18, 0x0E, 
	0x00, 0x18, 0x18, 0x18, 0x00, 0x18, 0x18, 0x18, 
	0x00, 0x70, 0x18, 0x18, 0x0C, 0x18, 0x18, 0x70, 
	0x00, 0x00, 0x00, 0x3A, 0x6C, 0x00, 0x00, 0x00, 
	0x00, 0x08, 0x1C, 0x36, 0x63, 0x41, 0x41, 0x7F,
};

