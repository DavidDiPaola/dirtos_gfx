/* 2020 David DiPaola, licensed CC0 (public domain worldwide) */

#include <stdint.h>

#include <stdio.h>

#include <SDL.h>

#include "mth/mth.h"

#include "gfx/gfx.h"

struct sdl {
	SDL_Window   * window;
	SDL_Renderer * renderer;
};

static const int _width  = 400;
static const int _height = 240;

static int
_init(struct sdl * sdl) {
	SDL_Init(SDL_INIT_VIDEO);

	sdl->window = SDL_CreateWindow(
		/*title=*/ "window title",
		/*x=*/ SDL_WINDOWPOS_CENTERED,
		/*y=*/ SDL_WINDOWPOS_CENTERED,
		/*w=*/ _width,
		/*h=*/ _height,
		/*flags=*/ 0
	);
	if (!sdl->window) {
		printf("ERROR: SDL_CreateWindow() failed: %s" "\n", SDL_GetError());
		return -1;
	}

	sdl->renderer = SDL_CreateRenderer(
		/*window=*/ sdl->window,
		/*index=*/ -1,
		/*flags=*/ SDL_RENDERER_ACCELERATED
	);
	if (!sdl->renderer) {
		printf("ERROR: SDL_CreateRenderer() failed: %s" "\n", SDL_GetError());
		return -2;
	}
	SDL_RenderClear(sdl->renderer);

	return 0;
}

static void
_close(struct sdl * sdl) {
	SDL_DestroyRenderer(sdl->renderer);
	sdl->renderer = NULL;

	SDL_DestroyWindow(sdl->window);
	sdl->window = NULL;

	SDL_Quit();
}

static SDL_Surface *
_sdl_surface_create(void) {
	SDL_Surface * surface = SDL_CreateRGBSurfaceWithFormat(
		/*flags=*/ 0,
		/*width=*/ _width,
		/*height=*/ _height,
		/*depth=*/ 16,
		/*format=*/ SDL_PIXELFORMAT_RGB565
	);
	if (!surface) {
		printf("ERROR: SDL_CreateRGBSurfaceWithFormat() failed: %s" "\n", SDL_GetError());
		return NULL;
	}

	int status = SDL_FillRect(
		/*dst=*/ surface,
		/*rect=*/ NULL,
		/*color=*/ 0x0000
	);
	if (status != 0) {
		printf("ERROR: SDL_FillRect() failed: %s" "\n", SDL_GetError());
		return NULL;
	}

	return surface;
}

static int
_sdl_surface_blit(struct sdl * sdl, SDL_Surface * surface) {
	SDL_Texture * texture = SDL_CreateTextureFromSurface(
		/*renderer=*/ sdl->renderer,
		/*surface=*/ surface
	);
	if (!texture) {
		printf("ERROR: SDL_CreateTextureFromSurface() failed: %s" "\n", SDL_GetError());
		return -1;
	}

	int status = SDL_RenderCopy(
		/*renderer=*/ sdl->renderer,
		/*texture=*/ texture,
		/*srcrect=*/ NULL,
		/*dstrect=*/ NULL
	);
	if (status != 0) {
		printf("ERROR: SDL_RenderCopy() failed: %s" "\n", SDL_GetError());
		return -2;
	}

	SDL_DestroyTexture(texture);

	return 0;
}

int
main() {
	struct sdl sdl;
	int status;

	status = _init(&sdl);
	if (status < 0) {
		return 1;
	}

	gfx_init(NULL, _width, _height);
	gfx_3d_init(/*fov=*/90);

	float pos_x = 0.0f;
	float pos_y = 0.0f;

	int quit = 0;
	int redraw = 1;
	while (!quit) {
		if (redraw) {
			SDL_RenderClear(sdl.renderer);

			SDL_Surface * surface = _sdl_surface_create();
			if (!surface) {
				return 2;
			}
			gfx_init(surface->pixels, surface->w, surface->h);



			struct mth_v2f pos[] = {
				{ .x=-0.4f+pos_x, .y=-0.3f+pos_y },
				{ .x= 0.4f+pos_x, .y=-0.4f+pos_y },
				{ .x= 0.0f+pos_x, .y= 0.4f+pos_y }
			};
			gfx_v2d_triangle(pos, gfx_color(0xFF,0x00,0x00), 1);



			status = _sdl_surface_blit(&sdl, surface);
			if (status < 0) {
				return 3;
			}
			redraw = 0;

			SDL_FreeSurface(surface);
			surface = NULL;

			SDL_RenderPresent(sdl.renderer);
		}

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				quit = 1;
				break;
			}
			else if (event.type == SDL_KEYDOWN) {
				switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						quit = 1;
						break;
					case SDLK_LEFT:
						pos_x -= 0.01f;
						redraw = 1;
						break;
					case SDLK_RIGHT:
						pos_x += 0.01f;
						redraw = 1;
						break;
					case SDLK_UP:
						pos_y += 0.01f;
						redraw = 1;
						break;
					case SDLK_DOWN:
						pos_y -= 0.01f;
						redraw = 1;
						break;
				}
			}
		}
	}

	_close(&sdl);
	return 0;
}

