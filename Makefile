# 2020 David DiPaola, licensed CC0 (public domain worldwide)

SRC_C = main.c gfx/gfx.c mth/mth.c
BIN = gfx_test

_CFLAGS = \
	-std=c99 -fwrapv \
	-Wall -Wextra \
	-g \
	$(shell sdl2-config --cflags) \
	$(CFLAGS)
_LDFLAGS = \
	-O1 \
	$(LDFLAGS)
_LDFLAGS_LIB = \
	$(shell sdl2-config --libs) \
	-lm

OBJ = $(SRC_C:.c=.o)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) $^ -o $@ $(_LDFLAGS_LIB)

