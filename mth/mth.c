#include <math.h>  /* TODO platform-specific ifdef */

#include "mth.h"

/* TODO platform-specific implementation */
float
mth_abs_f(float value) {
	return fabsf(value);
}

/* TODO platform-specific implementation */
float
mth_ceil_f(float value) {
	return ceilf(value);
}

/* TODO platform-specific implementation */
float
mth_cos_f(float radians) {
	return cosf(radians);
}

float
mth_degtorad_f(float degrees) {
	return degrees * (mth_PI_f / 180.0f);
}

int
mth_isequal_f(float a, float b) {
        return (mth_abs_f(a - b) < 0.000001f);
}

void
mth_mult_m4f(
	const struct mth_m4f * a, const struct mth_m4f * b,
	struct mth_m4f * out_result
) {
	/* From https://en.wikipedia.org/wiki/Matrix_multiplication#Definition */
	float
		a_1_1=mth_m4f_AT_ptr(a,0,0),  a_1_2=mth_m4f_AT_ptr(a,0,1),  a_1_3=mth_m4f_AT_ptr(a,0,2),  a_1_4=mth_m4f_AT_ptr(a,0,3),
		a_2_1=mth_m4f_AT_ptr(a,1,0),  a_2_2=mth_m4f_AT_ptr(a,1,1),  a_2_3=mth_m4f_AT_ptr(a,1,2),  a_2_4=mth_m4f_AT_ptr(a,1,3),
		a_3_1=mth_m4f_AT_ptr(a,2,0),  a_3_2=mth_m4f_AT_ptr(a,2,1),  a_3_3=mth_m4f_AT_ptr(a,2,2),  a_3_4=mth_m4f_AT_ptr(a,2,3),
		a_4_1=mth_m4f_AT_ptr(a,3,0),  a_4_2=mth_m4f_AT_ptr(a,3,1),  a_4_3=mth_m4f_AT_ptr(a,3,2),  a_4_4=mth_m4f_AT_ptr(a,3,3)
	;
	float
		b_1_1=mth_m4f_AT_ptr(b,0,0),  b_1_2=mth_m4f_AT_ptr(b,0,1),  b_1_3=mth_m4f_AT_ptr(b,0,2),  b_1_4=mth_m4f_AT_ptr(b,0,3),
		b_2_1=mth_m4f_AT_ptr(b,1,0),  b_2_2=mth_m4f_AT_ptr(b,1,1),  b_2_3=mth_m4f_AT_ptr(b,1,2),  b_2_4=mth_m4f_AT_ptr(b,1,3),
		b_3_1=mth_m4f_AT_ptr(b,2,0),  b_3_2=mth_m4f_AT_ptr(b,2,1),  b_3_3=mth_m4f_AT_ptr(b,2,2),  b_3_4=mth_m4f_AT_ptr(b,2,3),
		b_4_1=mth_m4f_AT_ptr(b,3,0),  b_4_2=mth_m4f_AT_ptr(b,3,1),  b_4_3=mth_m4f_AT_ptr(b,3,2),  b_4_4=mth_m4f_AT_ptr(b,3,3)
	;
	struct mth_m4f * m = out_result;
	mth_m4f_AT_ptr(m,0,0) = (a_1_1*b_1_1) + (a_1_2*b_2_1) + (a_1_3*b_3_1) + (a_1_4*b_4_1);
	mth_m4f_AT_ptr(m,0,1) = (a_1_1*b_1_2) + (a_1_2*b_2_2) + (a_1_3*b_3_2) + (a_1_4*b_4_2);
	mth_m4f_AT_ptr(m,0,2) = (a_1_1*b_1_3) + (a_1_2*b_2_3) + (a_1_3*b_3_3) + (a_1_4*b_4_3);
	mth_m4f_AT_ptr(m,0,3) = (a_1_1*b_1_4) + (a_1_2*b_2_4) + (a_1_3*b_3_4) + (a_1_4*b_4_4);
	mth_m4f_AT_ptr(m,1,0) = (a_2_1*b_1_1) + (a_2_2*b_2_1) + (a_2_3*b_3_1) + (a_2_4*b_4_1);
	mth_m4f_AT_ptr(m,1,1) = (a_2_1*b_1_2) + (a_2_2*b_2_2) + (a_2_3*b_3_2) + (a_2_4*b_4_2);
	mth_m4f_AT_ptr(m,1,2) = (a_2_1*b_1_3) + (a_2_2*b_2_3) + (a_2_3*b_3_3) + (a_2_4*b_4_3);
	mth_m4f_AT_ptr(m,1,3) = (a_2_1*b_1_4) + (a_2_2*b_2_4) + (a_2_3*b_3_4) + (a_2_4*b_4_4);
	mth_m4f_AT_ptr(m,2,0) = (a_3_1*b_1_1) + (a_3_2*b_2_1) + (a_3_3*b_3_1) + (a_3_4*b_4_1);
	mth_m4f_AT_ptr(m,2,1) = (a_3_1*b_1_2) + (a_3_2*b_2_2) + (a_3_3*b_3_2) + (a_3_4*b_4_2);
	mth_m4f_AT_ptr(m,2,2) = (a_3_1*b_1_3) + (a_3_2*b_2_3) + (a_3_3*b_3_3) + (a_3_4*b_4_3);
	mth_m4f_AT_ptr(m,2,3) = (a_3_1*b_1_4) + (a_3_2*b_2_4) + (a_3_3*b_3_4) + (a_3_4*b_4_4);
	mth_m4f_AT_ptr(m,3,0) = (a_4_1*b_1_1) + (a_4_2*b_2_1) + (a_4_3*b_3_1) + (a_4_4*b_4_1);
	mth_m4f_AT_ptr(m,3,1) = (a_4_1*b_1_2) + (a_4_2*b_2_2) + (a_4_3*b_3_2) + (a_4_4*b_4_2);
	mth_m4f_AT_ptr(m,3,2) = (a_4_1*b_1_3) + (a_4_2*b_2_3) + (a_4_3*b_3_3) + (a_4_4*b_4_3);
	mth_m4f_AT_ptr(m,3,3) = (a_4_1*b_1_4) + (a_4_2*b_2_4) + (a_4_3*b_3_4) + (a_4_4*b_4_4);
}

void
mth_mult_v4f_m4f(
	const struct mth_v4f * v, const struct mth_m4f * m,
	struct mth_v4f * out_result
) {
	/* Matrix multiplication: 1x4 * 4x4 */
	struct mth_v4f * r = out_result;
	r->x = (v->x*mth_m4f_AT_ptr(m,0,0)) + (v->y*mth_m4f_AT_ptr(m,1,0)) + (v->z*mth_m4f_AT_ptr(m,2,0)) + (v->w*mth_m4f_AT_ptr(m,3,0));
	r->y = (v->x*mth_m4f_AT_ptr(m,0,1)) + (v->y*mth_m4f_AT_ptr(m,1,1)) + (v->z*mth_m4f_AT_ptr(m,2,1)) + (v->w*mth_m4f_AT_ptr(m,3,1));
	r->z = (v->x*mth_m4f_AT_ptr(m,0,2)) + (v->y*mth_m4f_AT_ptr(m,1,2)) + (v->z*mth_m4f_AT_ptr(m,2,2)) + (v->w*mth_m4f_AT_ptr(m,3,2));
	r->w = (v->x*mth_m4f_AT_ptr(m,0,3)) + (v->y*mth_m4f_AT_ptr(m,1,3)) + (v->z*mth_m4f_AT_ptr(m,2,3)) + (v->w*mth_m4f_AT_ptr(m,3,3));
}

float
mth_radtodeg_f(float radians) {
	return radians * (180.0f / mth_PI_f);
}

/* TODO platform-specific implementation */
float
mth_round_f(float value) {
	return roundf(value);
}

/* TODO platform-specific implementation */
float
mth_sin_f(float radians) {
	return sinf(radians);
}

/* TODO platform-specific implementation */
float
mth_sqrt_f(float value) {
	return sqrtf(value);
}

/* TODO platform-specific implementation */
float
mth_tan_f(float radians) {
	return tanf(radians);
}




void
mth_v3f_cross(
        const struct mth_v3f * a, const struct mth_v3f * b,
        struct mth_v3f * out_result
) {
	float a_x=a->x, a_y=a->y, a_z=a->z;
	float b_x=b->x, b_y=b->y, b_z=b->z;

	/* From Wikipedia (https://en.wikipedia.org/wiki/Cross_product#Coordinate_notation) */
	out_result->x = (a_y*b_z) - (a_z*b_y);
	out_result->y = (a_z*b_x) - (a_x*b_z);
	out_result->z = (a_x*b_y) - (a_y*b_x);
}

void
mth_v3f_dot(
	const struct mth_v3f * a, const struct mth_v3f * b,
	float * out_result
) {
	/* From Wikipedia (https://en.wikipedia.org/wiki/Dot_product#Algebraic_definition) */
	(*out_result) = (a->x*b->x) + (a->y*b->y) + (a->z*b->z);
}

float
mth_v3f_magnitude(const struct mth_v3f * v) {
	return sqrtf((v->x*v->x) + (v->y*v->y) + (v->z*v->z));
}

int
mth_v3f_normal(
	const struct mth_v3f * v,
	struct mth_v3f * out_result
) {
	float v_magnitude = mth_v3f_magnitude(v);
	if (mth_isequal_f(v_magnitude, 0.0f)) {
		return -1;
	}

	out_result->x = v->x / v_magnitude;
	out_result->y = v->y / v_magnitude;
	out_result->z = v->z / v_magnitude;
	return 0;
}


