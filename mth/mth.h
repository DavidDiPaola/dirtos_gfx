#ifndef _MTH_H
#define _MTH_H

#define mth_PI_f 3.1415926536f

#define mth_m4f_AT_ptr(m_ptr, row, col) (m_ptr)->values[(col*4)+row]

struct mth_v2f {
	float x;
	float y;
};

struct mth_v3f {
	float x;
	float y;
	float z;
};

struct mth_v4f {
	float x;
	float y;
	float z;
	float w;
};

struct mth_m4f {
	float values[4*4];  /* column-major */
};

float
mth_abs_f(float value);

float
mth_ceil_f(float value);

float
mth_cos_f(float radians);

float
mth_degtorad_f(float degrees);

int
mth_isequal_f(float a, float b);

void
mth_mult_m4f(
	const struct mth_m4f * a, const struct mth_m4f * b,
	struct mth_m4f * out_result
);

void
mth_mult_v4f_m4f(
	const struct mth_v4f * v, const struct mth_m4f * m,
	struct mth_v4f * out_result
);

float
mth_round_f(float value);

float
mth_radtodeg_f(float radians);

float
mth_sin_f(float radians);

float
mth_sqrt_f(float value);

float
mth_tan_f(float radians);



void
mth_v3f_cross(
	const struct mth_v3f * a, const struct mth_v3f * b,
	struct mth_v3f * out_result
);

void
mth_v3f_dot(
	const struct mth_v3f * a, const struct mth_v3f * b,
	float * out_result
);

float
mth_v3f_magnitude(const struct mth_v3f * v);

int
mth_v3f_normal(
	const struct mth_v3f * v,
	struct mth_v3f * out_result
);



#endif

